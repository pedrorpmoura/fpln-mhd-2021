"""
Exercicio:
	1. ler um numero
	2. testar se o numero é par


1. Primeira versão
Primeiro temos de ler um número.
Se for par, então escrever a frase "É par".
Senão, escrever "É ímpar".


2. Segunda versão
Primeiro temos de ler um número.
Se for divisível por 2, então escrever a frase "É par".
Senão, escrever "É ímpar".

3. Terceira versão
Primeiro temos de ler um número.
Se o resto da divisão do número por 2 for igual a 0,
	então escrever a frase "É par".
Senão, escrever "É ímpar".
"""

numero = int(input('Insira um numero: '))

if (numero % 2) == 0:
	print("É par!")
	print("É mesmo par!")

else:	# executa se não for par
	print("É ímpar!")
	print("É mesmo ímpar!")


if (numero % 3) == 0:
	print("também é divisivel por 3")
else:
	print("não é divisivel por 3")

