

def procura(x, lista):
	for e in lista:
		if e == x:
			return True

	return False


print(procura(30, [1, 20, 13, 43]))


def conta_raparigas(lista_fm):
	raparigas = 0
	for e in lista_fm:
		if e == 'F':
			raparigas = raparigas + 1

	return raparigas


l = ['F', 'M', 'F', 'F', 'M', 'F', 'M']
n = conta_raparigas(l)

# print(n)


def conta_fm(lista_fm):
	raparigas = 0
	rapazes = 0
	indefinidos = 0

	for e in lista_fm:
		if e == 'F' or e == 'f':
			raparigas = raparigas + 1
		elif e == 'M' or e == 'm':
			rapazes = rapazes + 1
		else:
			indefinidos = indefinidos + 1


	return (raparigas, rapazes, indefinidos)


l = ['F', 'abc', 'm', 'F', 'f', 'M', 'F', 'M', 1234]
(n_raparigas, n_rapazes, n_indefinidos) = conta_fm(l)

print('Raparigas: ' + str(n_raparigas))
print('Rapazes: ' + str(n_rapazes))
print('Indefinidos: ' + str(n_indefinidos))





