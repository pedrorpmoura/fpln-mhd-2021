import matplotlib.pyplot as plt
from reader import read_csv_contents

rows = read_csv_contents('gold_data.csv')
rows = rows[1:]

precos = []
for tuplo in rows:
	preco = float(tuplo[1])
	precos.append(preco)

plt.plot(precos)
plt.show()