# LISTAS
l = ['a', 'b', 'c']
l2 = ['b', 'a', 'c']

print(l == l2)

# Consultar elementos
print(l[0])
print(l[1:])

# Alterar elementos
l[0] = 'd'
print(l)

# Tamanho da lista
print(len(l))

# Inverter a lista
l.reverse()
print(l)

print('PROCURA')
print('b' in l)
print('f' in l)



# TUPLOS
t = ('a', 'b', 'c')
print('\n\n\n\nTUPLOS')

# Consultar elementos
print(t[0])
print(t[1:])

print(len(t))


# CONJUNTOS
print('\n\n\nCONJUNTOS')

s = {'a', 'a', 'c', 'b'}

s.add('e')
print(s)

s.remove('e')
print(s)
