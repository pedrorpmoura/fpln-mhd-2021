def conta_fm(lista_fm):
	raparigas = 0
	rapazes = 0
	indefinidos = 0

	for e in lista_fm:
		if e == 'F' or e == 'f':
			raparigas = raparigas + 1
		elif e == 'M' or e == 'm':
			rapazes = rapazes + 1
		else:
			indefinidos = indefinidos + 1


	return (raparigas, rapazes, indefinidos)


pessoas = []
pessoa = input('Pessoa: ')

# while pessoa != 'q' and pessoa != 'Q' and pessoa != 'quit' :
while pessoa not in {'q', 'Q', 'quit', 'QUIT'}:
	pessoas.append(pessoa)

	pessoa = input('Pessoa: ')

(n_raparigas, n_rapazes, n_indefinidos) = conta_fm(pessoas)

print('Raparigas: ' + str(n_raparigas))
print('Rapazes: ' + str(n_rapazes))
print('Indefinidos: ' + str(n_indefinidos))

