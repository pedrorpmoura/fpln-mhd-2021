# Proposta de exercício

1. Criem uma estrutura com informação completa de alunos de uma turma (nome, notas, idade, sexo, ... o que quiserem)
Sugestão:
```python
alunos = {
	"A1": {
		"nome": string
		"idade": int
		"sexo": string,
		"notas": [ int ],
		"curso": string
	},
	...
}

```

2. Escrevam programas que resolvam as seguintes alíneas:
	- Contar quantos alunos existem.
	- Escrever o nome do aluno mais velho e do mais novo.
	- Escrever a distribuição por sexo da turma.
	- Escrever o nome de cada aluno e a sua média de notas.
	- Escrever quantos alunos pertencem a um certo curso.
	- Escrever a média de notas de TODA a turma.

3. Para complicar a estrutura mais um bocado, o campo notas não deverá ser uma lista de inteiros, mas uma lista de dicionários que têm o seguinte formato (exemplo):
```python
{
	"UC": "fpln",
	"nota": 14
}
```
Voltem a resolver as perguntas relacionadas com as notas.
E também:
	- Calcular a média de notas de uma UC.






