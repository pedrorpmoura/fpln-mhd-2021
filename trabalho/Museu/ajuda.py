import glob
import re
import os


def obter_ficheiros_bi() -> list:
	"""
	Função que devolve uma lista com os caminhos de todos
	os ficheiros 'bi.xml' do museu.
	"""
	cwd = os.getcwd()
	return glob.glob(cwd + '/*/bi.xml')



def le_ficheiro(caminho_ficheiro: str) -> str:
	"""
	Função que lê um ficheiro e retorna o conteúdo lido.

	Argumentos
		caminho_ficheiro - caminho para o ficheiro que queremos ler.
	"""
	f = open(caminho_ficheiro, 'r')
	conteudo = f.read()
	f.close()

	return conteudo



def obter_nome(bi_xml: str) -> str:
	"""
	Função que recebe uma string com o conteudo do xml do BI e devolve o
	nome da pessoa identificada nesse BI.

	Argumentos
		bi_xml - string com o conteudo xml do BI
	"""
	if m := re.search(r'<nome>((.|\n)+)</nome>', bi_xml):
		return m[1].strip()


def obter_bio(bi_xml: str) -> str:
	"""
	Função que recebe uma string com o conteudo do xml do BI e devolve a
	biografia da pessoa identificada nesse BI.

	Argumentos
		bi_xml - string com o conteudo xml do BI
	"""
	if m := re.search(r'<biografia>((.|\n)+)</biografia>', bi_xml):
		return m[1].strip()


def obter_data_nasc(bi_xml: str) -> str:
	"""
	Função que recebe uma string com o conteudo do xml do BI e devolve a
	data de nascimento da pessoa identificada nesse BI.
	No caso de não existir essa informação, a função retorna a string
	"Sem data de nascimento"

	Argumentos
		bi_xml - string com o conteudo xml do BI
	"""
	if m := re.search(r'<data_nasc[ ]+iso="(.+)"/>', bi_xml):
		return m[1].strip()
	else:
		return 'Sem data de nascimento'


def obter_data_morte(bi_xml: str) -> str:
	"""
	Função que recebe uma string com o conteudo do xml do BI e devolve a
	data de morte da pessoa identificada nesse BI.
	No caso de não existir essa informação, a função retorna a string
	"Sem data de morte"

	Argumentos
		bi_xml - string com o conteudo xml do BI
	"""
	if m := re.search(r'<data_morte[ ]+iso="(.+)"/>', bi_xml):
		return m[1].strip()
	else:
		return 'Sem data de morte'


def obter_local_nasc(bi_xml: str) -> str:
	"""
	Função que recebe uma string com o conteudo do xml do BI e devolve o
	local de nascimento da pessoa identificada nesse BI.
	No caso de não existir essa informação, a função retorna a string
	"Sem local de nascimento"

	Argumentos
		bi_xml - string com o conteudo xml do BI
	"""
	if m := re.search(r'<local_nasc>((.|\n)+)</local_nasc>', bi_xml):
		return m[1].strip()
	else:
		return 'Sem local de nascimento'


def obter_profissao(bi_xml: str) -> str:
	"""
	Função que recebe uma string com o conteudo do xml do BI e devolve a
	profissao da pessoa identificada nesse BI.
	No caso de não existir essa informação, a função retorna a string
	"Sem profissao"

	Argumentos
		bi_xml - string com o conteudo xml do BI
	"""
	if m := re.search(r'<profissao>((.|\n)+)</profissao>', bi_xml):
		return m[1].strip()
	else:
		return 'Sem profissao'



def compara_datas(data1: str, data2: str) -> int:
	"""
	Função que compara 2 datas, e retorna informação acerca de 
	qual data é a mais antiga.

	Argumentos:
		data1, data2 - as datas que queremos comparar.
					   devem estar no formato 'YYYY-MM-DD'.

	Retorna:
		-1 se a data1 é mais antiga que a data2
		0 se a data1 for igual a data2
		1 se a data1 for mais recente que a data2
	"""

	ano_d1 = int(data1[:4])
	ano_d2 = int(data2[:4])
	if ano_d1 < ano_d2:
		return -1
	elif ano_d1 > ano_d2:
		return 1
	else:
		mes_d1 = int(data1[5:7])
		mes_d2 = int(data2[5:7])
		if mes_d1 < mes_d2:
			return -1
		elif mes_d1 > mes_d2:
			return 1
		else:
			dia_d1 = int(data1[8:])
			dia_d2 = int(data2[8:])
			if dia_d1 < dia_d2:
				return -1
			elif dia_d1 > dia_d2:
				return 1
			else:
				return 0


