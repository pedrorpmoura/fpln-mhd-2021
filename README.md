# MHD 21/22 - Fundamentos em Processamento de Linguagem Natural

Repositório das aulas de Fundamentos PLN

## Docente

Pedro Moura (pedrorpmoura@gmail.com)

## Plano de Estudos

1. Introdução à Programação
    - Noção de algoritmo e de programa
    - Valores, variáveis e expressões
    - Tipos de dados básicos
    - Funções
    - Estruturas condicionais e cíclicas
    - Input/Output
    - Estruturas de dados
2. Processamento de linguagens baseado em padrões
    - Expressões regulares
    - Extração de informação a partir de textos
    - Transformações de texto


## Sumários

| Aula | Data  | Sumário |
| ---- | ----- | ------- |
| 1    | 08/10 | Introdução à programação. Noção de algoritmo e de programa. |
| 2    | 15/10 | Introdução à programação em Python. Strings.|
| 3    | 22/10 | Operações aritméticas. Estruturas de controlo: condicionais. |
| 4    | 29/10 | Estruturas de controlo: ciclo While. |
| 5    | 12/11 | Ciclo For. Funções. |
| 6    | 19/11 | Estruturas de dados: Listas, tuplos e conjuntos. |
| 7    | 26/11 | Resolução de um exercício de forma a treinar as estruturas lecionadas na aula anterior. |
| 8    | 03/12 | Estruturas de dados: Dicionários. |
| 9    | 10/12 | Resolução de um exercício de forma a treinar dicionários. |
| 10   | 17/12 | Funções Recursivas. |
| 11   | 07/01 | Sessão de dúvidas para o trabalho prático e para o teste. |
| 12   | 14/01 | Teste. |


## Avaliação

Avaliação contínua:
- 1 teste:
    - 14 de janeiro
- 1 trabalho prático


