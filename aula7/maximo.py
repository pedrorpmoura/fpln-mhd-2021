from reader import read_csv_contents

def maximo(lista):
	vmax = 0
	for e in lista:
		if e > vmax:
			vmax = e

	return vmax


rows = read_csv_contents('gold_data.csv')
rows = rows[1:]

precos = []
for tuplo in rows:
	preco = float(tuplo[1])
	precos.append(preco)

preco_maximo = maximo(precos)

data_preco_maximo = ''
for tuplo in rows:
	preco = float(tuplo[1])
	if preco == preco_maximo:
		data_preco_maximo = tuplo[0]
		break

print(data_preco_maximo)
print(preco_maximo)

