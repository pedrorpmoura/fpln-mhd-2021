import csv

def read_csv_contents(filename):
	"""
	Função que recebe o nome de um ficheiro csv, e transforma o seu
	conteúdo para uma lista de tuplos, onde cada tuplo corresponde a
	uma linha do ficheiro csv.
	"""
	file = open(filename, 'r')

	csvreader = csv.reader(file)

	rows = []
	for row in csvreader:
		rows.append(tuple(row))

	return rows
