turma = {
	"A1": {
		"nome": "Madalena Barros",
		"idade": 21,
		"sexo": 'f',
		"notas": [20,20,20,20,20],
		"curso": "LA"
	},

	"A2": {
		"nome": "Ricardo Ribeiro",
		"idade": 21,
		"sexo": "M",
		"notas": [9, 7, 16, 20, 18],
		"curso": "LLE"
	},

	"A3": {
		"nome": "Maria Sá",
		"idade": 22,
		"sexo": "F",
		"notas": [9,9,9,9,9],
		"curso": "LLE"
	},

	"A4": {
		"nome": "Fábio Silva",
		"idade": 27,
		"sexo": "Masculino",
		"notas": [15,16,17,18,19],
		"curso": "LLE"
	},

	"A5": {
		"nome": "Snizhana Umanets",
		"idade": 23,
		"sexo": "Feminino",
		"notas": [12,15,14,16,20],
		"curso": "ML"
	}
}



def conta_alunos(turma):
	contador = 0

	for aluno in turma:
		contador = contador + 1

	return contador


def aluno_mais_novo(turma):
	minimo = 99
	nome_mais_novo = ""
	idade_mais_novo = 0
	for aluno in turma:
		info_aluno = turma[aluno]
		idade = info_aluno['idade']

		if idade < minimo:
			minimo = idade
			nome_mais_novo = info_aluno['nome']
			idade_mais_novo = info_aluno['idade']

	return (nome_mais_novo, idade_mais_novo)


def aluno_mais_velho(turma):
	maximo = 0
	nome_mais_velho = ""
	idade_mais_velho = 0
	for aluno in turma:
		info_aluno = turma[aluno]
		idade = info_aluno['idade']

		if idade > maximo:
			maximo = idade
			nome_mais_velho = info_aluno['nome']
			idade_mais_velho = info_aluno['idade']

	return (nome_mais_velho, idade_mais_velho)



def distribuicao_sexo(turma):
	dist = {
		'masculino': 0,
		'feminino': 0,
		'outro': 0
	}
	
	for aluno in turma:
		genero = turma[aluno]['sexo']

		if genero == 'F' or genero == 'Feminino' or genero == 'f':
			dist['feminino'] = dist['feminino'] + 1
		elif genero in {'M', 'm', 'Masculino', 'masculino'}:
			dist['masculino'] = dist['masculino'] + 1
		else:
			dist['outro'] = dist['outro'] + 1

	return dist




def media(lista: list):
	soma = 0
	n_elementos = 0
	for n in lista:
		soma = soma + n
		n_elementos += 1

	return soma / n_elementos


def media_aluno(turma: dict):
	lista_alunos_e_a_sua_media = []

	for aluno in turma:
		notas = turma[aluno]['notas']
		media_aluno = media(notas)

		nome = turma[aluno]['nome']

		lista_alunos_e_a_sua_media.append((nome, media_aluno))

	return lista_alunos_e_a_sua_media




def media_turma(turma):
	notas_de_toda_a_turma: list = []

	for aluno in turma:
		notas = turma[aluno]['notas']

		# mesma coisa que
		# notas_de_toda_a_turma = notas_de_toda_a_turma + notas
		notas_de_toda_a_turma += notas

	return media(notas_de_toda_a_turma)

print(media_turma(turma))
