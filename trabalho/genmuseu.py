import sys
import re
import os


def f_remove_accents(old):
    """
    Removes common accent characters, lower form.
    Uses: regex.
    """
    new = old.lower()
    new = re.sub(r'[àáâãäå]', 'a', new)
    new = re.sub(r'[èéêë]', 'e', new)
    new = re.sub(r'[ìíîï]', 'i', new)
    new = re.sub(r'[òóôõö]', 'o', new)
    new = re.sub(r'[ùúûü]', 'u', new)
    return new


with open('dataset.xml', 'r') as f:
	content = f.read()


	header = '''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE bi SYSTEM "bi.dtd">'''

	bis = re.findall(r'<bi>(?:.|\n)*?</bi>', content)

	for bi in bis:
		bi = re.sub('\n\t', '\n', bi)
		name = re.search(r'<nome>((?:.|\n)+)</nome>', bi)[1]
		name = f_remove_accents(name.strip())
		name = re.sub(' ', '-', name)

		bi_content = header + '\n' + bi

		bi_dir = 'Museu/' + name 
		if not os.path.exists(bi_dir):
			os.mkdir(bi_dir)

		with open(f'{bi_dir}/bi.xml', 'w') as bi_file:
			bi_file.write(bi_content)







