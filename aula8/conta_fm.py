

def procura(x, lista):
	for e in lista:
		if e == x:
			return True

	return False


def conta_raparigas(lista_fm):
	raparigas = 0
	for e in lista_fm:
		if e == 'F':
			raparigas = raparigas + 1

	return raparigas


l = ['F', 'M', 'F', 'F', 'M', 'F', 'M']
n = conta_raparigas(l)

# print(n)


def conta_fm(lista_fm):

	info = {
		"raparigas": 0,
		"rapazes": 0,
		"indefinidos": 0
	}

	for e in lista_fm:
		if e == 'F' or e == 'f':
			info["raparigas"] = info["raparigas"] + 1
		elif e == 'M' or e == 'm':
			info["rapazes"] = info["rapazes"] + 1
		else:
			info["indefinidos"] = info["indefinidos"] + 1


	return (info["raparigas"], info["rapazes"], info["indefinidos"])


l = ['F', 'abc', 'm', 'F', 'f', 'M', 'F', 'M', 1234]
(n_raparigas, n_rapazes, n_indefinidos) = conta_fm(l)

print('Raparigas: ' + str(n_raparigas))
print('Rapazes: ' + str(n_rapazes))
print('Indefinidos: ' + str(n_indefinidos))





