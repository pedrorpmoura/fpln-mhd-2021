# import reader
from reader import read_csv_contents


def media(soma, n_elementos):
	return soma / n_elementos



rows = read_csv_contents('gold_data.csv')
rows = rows[1:] 
# del rows[0] # apaga o indice 0 da lista rows

# print(rows)

soma = 0
contador = 0
for tuplo in rows:
	preco = tuplo[1] # ir buscar o segundo elemento do tuplo
	
	soma = soma + float(preco) # transformar o preco (que é uma string)
							   # num numero real

	contador = contador + 1

print('Média:')
print(media(soma, contador))





