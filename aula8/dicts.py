
l = [1,2,3,4] # lista

t = (1,2,3,4) # tuplo

c = {1,2,3,4} # conjunto



aluno1 = ('Pedro', 'Moura', 23, 'LCC')
aluno2 = ('Sara', 'Almeida', 23, 'Nutrição')

alunos = [ aluno1, aluno2 ]


alunos = { 
	"A82258": {
		"primeiro_nome": "Pedro",
		"ultimo_nome": "Moura",
		"idade": 23,
		"curso": "LCC",
		"notas": [ 14, 12, 20, 19, 15 ]
	}, 
	"A99999": {
		"primeiro_nome": "Sara",
		"ultimo_nome": "Almeida",
		"idade": 23,
		"curso": "Nutrição",
		"notas": [ 16, 16, 17, 18, 15 ]
	}
}


print(alunos["A82258"]["notas"])






# primeiros_nomes = []
# for chave in alunos:
# 	valor = alunos[chave]
# 	primeiros_nomes.append(valor["primeiro_nome"])

# print(primeiros_nomes)







