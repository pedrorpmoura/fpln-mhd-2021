

idade = 30


if idade > 18:
	print('é um adulto')

elif idade > 12:
	print('é um adolescente')

elif idade > 3:
	print('é uma criança')

else:
	print('é um bebé')


# if idade > 18:
# 	print('é um adulto')
# if idade > 12:
# 	print('é um adolescente')
# if idade > 3:
# 	print('é uma criança')
# else:
# 	print('é um bebé')

idade = 10

if idade > 18:
	print('é um adulto')

if idade > 12 and idade <= 18:
	print('é um adolescente')

if idade > 3 and idade <= 12:
	print('é uma criança')

if idade <= 3:
	print('é um bebé')

print('Decidido')






