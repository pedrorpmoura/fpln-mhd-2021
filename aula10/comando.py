

def f(x):
	return 2 * x


def usage_f():
	print("'f' tem de receber um número. E retorna o seu dobro")


# ler um input
n = input('Insira um número: ')
if n.isdigit():
	n = int(n)
	print(f(n))
else:
	usage_f()

