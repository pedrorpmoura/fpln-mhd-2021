turma = {
	"A1": {
		"nome": "Madalena Barros",
		"idade": 21,
		"sexo": 'f',
		"notas": [
			{
				"disciplina": "Ingles",
				"nota": 20
			},
			{
				"disciplina": "Portugues",
				"nota": 18
			}
		],
		"curso": "LA"
	},

	"A2": {
		"nome": "Ricardo Ribeiro",
		"idade": 21,
		"sexo": "M",
		"notas": [9, 7, 16, 20, 18],
		"curso": "LLE"
	},

	"A3": {
		"nome": "Maria Sá",
		"idade": 22,
		"sexo": "F",
		"notas": [9,9,9,9,9],
		"curso": "LLE"
	},

	"A4": {
		"nome": "Fábio Silva",
		"idade": 27,
		"sexo": "Masculino",
		"notas": [15,16,17,18,19],
		"curso": "LLE"
	},

	"A5": {
		"nome": "Snizhana Umanets",
		"idade": 23,
		"sexo": "Feminino",
		"notas": [12,15,14,16,20],
		"curso": "ML"
	}
}


print(turma['A1']['notas'][1]['disciplina'])



