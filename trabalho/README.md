# Trabalho Prático

## Descrição

Este trabalho assume que a estrutura do vosso projeto é a seguinte:
```
Museu/
	pessoa1/
		bi.xml
		...
		fotos/
			foto1.png
			...
	pessoa2/
		bi.xml
		...
		fotos/
			foto1.png
			...
	...
```
e que os ficheiros `bi.xml` seguem o seguinte DTD:
```xml
<?xml version="1.0" encoding="UTF-8"?>

<!ELEMENT bi (nome, biografia, data_nasc?, local_nasc?, data_morte?, profissao?, foto?)>
<!ELEMENT nome (#PCDATA)>
<!ELEMENT biografia (#PCDATA)>
<!ELEMENT data_nasc EMPTY>
    <!ATTLIST data_nasc iso CDATA #REQUIRED>
<!ELEMENT local_nasc (#PCDATA)>
<!ELEMENT data_morte EMPTY>
    <!ATTLIST data_morte iso CDATA #REQUIRED>
<!ELEMENT profissao (#PCDATA)>
<!ELEMENT foto (#PCDATA)>
    <!ATTLIST foto href CDATA #REQUIRED>
    <!ATTLIST foto legenda CDATA #REQUIRED>
```

Para o realizar, coloquem os ficheiros dentro da pasta *Museu*.


### Parte 1
Utilizando as funções presentes no ficheiro `ajuda.py`, crie funções que:
1. Imprima o nome de todas as pessoas do museu.
2. Imprima o nome e a profissão (caso exista) das pessoas que nasceram num determinado local. Caso a pessoa não tenha profissão, imprimir "Sem profissão".
3. Imprima o nome e local de nascimento das pessoas que nasceram num determinado intervalo de datas.
4. Imprimir uma distribuição com o número de pessoas nascidas em cada ano.
4. Imprima um dicionário com as informações relativas a uma determinada pessoa.
6. Imprimir algumas curiosidades, como:
	- Pessoa mais nova
	- Pessoa mais velha
	- Nome mais comprido
	- etc.



### Parte 2
Crie um programa interativo que permita ao utilizador tirar partido das funções que definiu na primeira parte.

