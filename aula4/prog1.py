"""
1. Ler um numero
2. Enquanto o numero for diferente que 0, volto a ler, e repito o passo 2

1 == 2 - testa se sao iguais
1 != 2 - testa se sao diferentes

"""


numero = int(input('Número: '))
soma = 0
n_elementos = 0

while numero != 0:
	soma = soma + numero
	print('soma atual: ' + str(soma))
	
	n_elementos = n_elementos + 1
	print('numero de elementos atual: ' + str(n_elementos))

	if soma >= 100:
		print('Arrebentei!')
		break

	numero = int(input('Número: '))


print('Acabei!')
print('Total: ' + str(soma))

media = soma / n_elementos
print('Média: ' + str(media))

